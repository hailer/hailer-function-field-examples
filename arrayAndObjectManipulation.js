const output = [];
let firstName;
let lastName;
let utcDate;
let person = {};
let assignment;
let dupeCheck;
let index;
let outputString = '';

/* This function is called from the forEach below,it checks if the output array has the person, if it does, it removes the person.
    This is intended to get the person "last in line" since in this case we want to know who lastly worked
    on this item, without there being the same person multiple times */
function removeDuplicates(person){
    dupeCheck = output.find(x =>
        x.firstName === person.firstName &&
        x.lastName === person.lastName
    );
    index = output.indexOf(dupeCheck);

    if (index != -1){
        output.splice(index,1);
    }

    output.push(person);
} 
/* This forEach iterates through all linked acitivities, and checks according to the processId if it's a specific process.
It then creates a "person" object from the users name, checks for duplicates, and adds it to the array */
row.$.linkedFrom.forEach(worker => {
    if (worker.processId === 'processIdfield') {
        let tekija = worker.fields['Employee'];
        let date = worker.fields['Date'] || null;
        assignment = worker.fields['Assignment'].name || null;

        assignment = assignment.toString().substring(0,4) || null;
        utcDate = new Date(date);
        firstName = tekija && tekija.firstname || '';
        lastName = tekija && tekija.lastname || '';
        person = {firstName, lastName, utcDate};

        removeDuplicates(person);  
    }
})

/* Here we iterate through the array we just created, and create a string of names accordingly,
 the "\n" means new line*/
output.forEach(worker => {
    outputString += worker.assignment +'- '+
                    worker.firstName + ' ' + 
                    worker.lastName +' '+
                    utcDate.getDate() + '/' +
                    (utcDate.getMonth() +1) + '\n';
})

return outputString;