let length = row['Length'];
let height = row['Height'];
let depth =  row['Depth'];
let linkedFrom = row.$.linkedFrom;


/* this is an easier way of writing a for loop, normally it would look like this: 
    for (element = 0; element < linkedFrom.length; element++) {
        someCode += element.fields..;
    }
*/
linkedFrom.forEach(element => {
    if (element.processId === 'processId'){
    /* 
    This code loops through all Linked Activities,
    if the activity corresponds with given processId,
    it assigns the given fields to "newLength, newHeight .."
    the "|| 0" makes sure newX is not null.
    */
            let newLength = element.fields['ChangeInLength'] || 0;
            let newHeight = element.fields['ChangeInHeight'] || 0;
            let newDepth = element.fields['ChangeInDepth'] || 0;

            ((newLength < 0) ? (length -= Math.abs(newLength)) : (length +=  newLength));
            ((newHeight < 0) ? (height -= Math.abs(newHeight)) : (height +=  newHeight));
            ((newDepth < 0) ? (depth -= Math.abs(newDepth)) : (depth +=  newDepth));

    /*
    These are called ternaries, they're simplified "if" statements
    
    ex:     (condition) ? do if true : do if else ;

    this example simply checks if newLength is positive or negative, 
    (0 is considered postive in this case)

    the function of this example is unnecessary, 
    "length += newLength;" works just aswell assuming newLength is not null or undefined
    this is only for demonstration purposes
    
     */
    }
});
let cubic = length*height*depth;

//this console log is if you want to run this code locally or your browser console
console.log(cubic);

return cubic;
