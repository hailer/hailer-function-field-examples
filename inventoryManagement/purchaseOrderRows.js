// PO Rows
const product = row['Product'] && row['Product'].name || 'Product name';
const quantity = row['Quantity'] || 0;

return quantity + ' x ' + product;


// Unit weight
return row['Unit weight'] || row['Product'] && row['Product'].fields['Unit_weight'] || null;


// Total price
const quantity = row['Quantity'] || 0;
const unitPrice = row['Unit price'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(quantity * unitPrice) || null;


// Total weight
const quantity = row['Quantity'] || 0;
const weight = row['Unit weight'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(quantity * weight) || null;


// Status
const orderPhase = row['Purchase order'] && row['Purchase order'].phase;

const phaseStatus = {
    '5f635ba50230632ff79c1228': 'Offered',
    '5f635ba50230632ff79c1229': 'Ordered',
    '5f635ba50230632ff79c122a': 'Received'
};

return phaseStatus[orderPhase] || 'Purchase order missing!';


// Received
return row['Purchase order'] && row['Purchase order'].fields['Received'] || null;


// partCatalogue
return row['Product'] && row['Product'].fields['partCatalogue'] || null;