// Purchase order
const supplier = row['Supplier'] && row['Supplier'].name || 'Supplier';

const offerDate = (
    row['Offer date'] &&
    new Date(row['Offer date']).toISOString().substr(0,10) ||
    'Offer date'
);

return (offerDate + '/PO/' + supplier);


// Total price
const allowedName = ['Purchase Order Rows'];

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Total_price'] ||
        0
    );
})

return round(total) || 0;


// Total weight
const allowedName = ['Purchase Order Rows'];

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Total_weight'] ||
        0
    );
})
    
return round(total) || 0;


// Time spent
const allowedName = ['Time tracker'];
    
function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Time_spent'] ||
        0
    );
})

return round(total) || 0;


// orderRows
const processName = ['Purchase Order Rows'];
const arrayOfObjects = [];

function stringify(data) {
    return JSON.stringify(data);
}

row.$.linkedFrom.forEach(item => {
    if (processName.includes(item.processName)){
        let object = {};
        object.name = item.name;
        object.activityId = item._id;
        object.processId = item.processId;
        object.phaseId = item.phaseId;
        object.purchaseOrder = (
            item.fields['Purchase_order'] &&
            item.fields['Purchase_order']._id  || ''
        );
        object.quantity = item.fields['Quantity'] || 0;
        object.product = (
            item.fields['Product'] &&
            item.fields['Product']._id  || ''
        );
        object.unitPrice = item.fields['Unit_price'] || 0;
        object.unitWeight = item.fields['Unit_weight'] || 0;
        arrayOfObjects.push(object);
    }
});

return stringify(arrayOfObjects);