// Unit net price
const allowedName = ['Purchase Order Rows'];
const allowedStatus = 'Received';
const delivered = row['Delivered (SO)'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

let totalCount = 0;
let totalPrice = 0;
let received = 0;
let quantity = 0;
let price = 0;
let lastPrice = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    if (
        allowedName.includes(activity.processName) &&
        allowedStatus.includes(activity.fields['Status'])
    ) {
        quantity = activity.fields['Quantity'] || 0;
        price = activity.fields['Total_price'] || 0;
        received += quantity;
        lastPrice = round(price / quantity) || null;
        if (quantity && price && (received > delivered)) {
            totalCount += quantity;
            totalPrice += price;
        }
    }
})

return (totalCount && round(totalPrice / totalCount)) || lastPrice || null;


// In stock
const received = row['Received (PO)'] || 0;
const delivered = row['Delivered (SO)'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(received - delivered) || 0;


// Stock value
const netPrice = row['Unit net price'] || 0;
const inStock = row['In stock'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return inStock > 0 && round(netPrice * inStock) || 0;


// Gross margin
const unitPrice = row['Unit price'] || 0;
const netPrice = row['Unit net price'] || 0;

return (
    netPrice &&
    unitPrice &&
    Math.round(100 * (unitPrice - netPrice) / unitPrice) ||
    null
);


// Offered (PO)
const allowedName = ['Purchase Order Rows'];
const allowedStatus = 'Offered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        0
    );
})

return round(total) || 0;


// Ordered (PO)
const allowedName = ['Purchase Order Rows'];
const allowedStatus = 'Ordered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        0
    );
})

return round(total) || 0;


// Received (PO)
const allowedName = ['Purchase Order Rows'];
const allowedStatus = 'Received';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        0
    );
})

return round(total) || 0;


// Offered (SO)
const allowedName = [
    'Sales Order Rows',
    'Part Catalogue'
];
const allowedStatus = 'Offered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        (activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        activity.fields[allowedStatus + '_(SO)']) ||
        0
    );
})

return round(total) || 0;


// Ordered (SO)
const allowedName = [
    'Sales Order Rows',
    'Part Catalogue'
];
const allowedStatus = 'Ordered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        (activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        activity.fields[allowedStatus + '_(SO)']) ||
        0
    );
})

return round(total) || 0;


// Delivered (SO)
const allowedName = [
    'Sales Order Rows',
    'Part Catalogue'
];
const allowedStatus = 'Delivered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        (activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        activity.fields[allowedStatus + '_(SO)']) ||
        0
    );
})

return round(total) || 0;