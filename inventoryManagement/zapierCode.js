// (01) Sales Order added to phase
const data = JSON.parse(inputData.data);
const fields = data.fields;

let orderRows = '';
Object.values(fields).forEach(value => {
    if (value.id === '6092a0e51cf9126ac0176843'){
        orderRows = JSON.parse(value.value);
    }
})

return orderRows.map(item => {
  return item;
});


// (02) Purchase Order added to phase
const data = JSON.parse(inputData.data);
const fields = data.fields;

let orderRows = '';
Object.values(fields).forEach(value => {
    if (value.id === '6092a1301cf91269c2176c1a'){
        orderRows = JSON.parse(value.value);
    }
})

return orderRows.map(item => {
  return item;
});


// (03) Sales or Purchase Order Rows updated
const data = JSON.parse(inputData.data);
const fields = data.fields;

let orderRows = '';
Object.values(fields).forEach(value => {
    if (value.id === '609bb8f181fdfcf8076f3047'){
        orderRows = JSON.parse(value.value);
    }
    if (value.id === '609bb8c281fdfc51c26f2da2'){
        orderRows = JSON.parse(value.value);
    }
})

return orderRows.map(item => {
  return item;
});
