// Unit weight
const allowedName = ['Part Catalogue'];
    
function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Total_weight'] ||
        0
    );
})

return round(total) || null;


// Unit net price
const allowedName = ['Part Catalogue'];

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Total_net_price'] ||
        0
    );
})

return round(total) || null;


// Gross margin
const unitPrice = row['Unit price'] || 0;
const netPrice = row['Unit net price'] || 0;

return (
    netPrice &&
    unitPrice &&
    Math.round(100 * (unitPrice - netPrice) / unitPrice) ||
    null
);


// Available
const allowedName = ['Part Catalogue'];

let total = null;

Object.values(row.$.linkedFrom).forEach(activity => {
    if (allowedName.includes(activity.processName)) {
        if (total === null || activity.fields['Possible_assemblies'] < total) {
            total = activity.fields['Possible_assemblies'];
        }
    }
})

return total || 0;


// Total value
const unitPrice = row['Unit price'] || 0;
const available = row['Available'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(unitPrice * available) || 0;


// Offered (SO)
const allowedName = ['Sales Order Rows'];
const allowedStatus = 'Offered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        0
    );
})

return round(total) || 0;


// Ordered (SO)
const allowedName = ['Sales Order Rows'];
const allowedStatus = 'Ordered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        0
    );
})

return round(total) || 0;


// Delivered (SO)
const allowedName = ['Sales Order Rows'];
const allowedStatus = 'Delivered';

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Status'] === allowedStatus &&
        activity.fields['Quantity'] ||
        0
    );
})

return round(total) || 0;


// partCatalogue
const processName = ['Part Catalogue'];
const arrayOfObjects = [];

function stringify(data) {
    return JSON.stringify(data);
}

row.$.linkedFrom.forEach(item => {
    if (processName.includes(item.processName)){
        let object = {};
        object.name = item.name;
        object.activityId = item._id;
        object.processId = item.processId;
        object.phaseId = item.phaseId;
        object.partOfProduct = (
            item.fields['Part_of_product'] &&
            item.fields['Part_of_product']._id  || ''
        );
        object.component = (
            item.fields['Component'] &&
            item.fields['Component']._id  || ''
        );
        object.quantity = item.fields['Quantity'] || 0;
        arrayOfObjects.push(object);
    }
});

return stringify(arrayOfObjects);