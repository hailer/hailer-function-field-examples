// SO Row
const product = row['Product'] && row['Product'].name || 'Product name';
const quantity = row['Quantity'] || 0;

return quantity + ' x ' + product;


// Unit net price
return (
    row['Unit net price'] ||
    (row['Product'] && row['Product'].fields['Unit_net_price']) ||
    null
);


// Unit price
return row['Unit price'] || (row['Product'] && row['Product'].fields['Unit_price']) || null;


// Unit weight
return row['Unit weight'] || row['Product'] && row['Product'].fields['Unit_weight'] || null;


// Total price
const quantity = row['Quantity'] || 0;
const unitPrice = row['Unit price'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(quantity * unitPrice) || null;


// Total weight
const quantity = row['Quantity'] || 0;
const weight = row['Unit weight'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(quantity * weight) || null;


// Status
const orderPhase = row['Sales order'] && row['Sales order'].phase;

const phaseStatus = {
    '5f634015e156db058438684b': 'Offered',
    '5f63405ce156db0584386c61': 'Ordered',
    '5f634062e156db0584386ce5': 'Delivered'
};

return phaseStatus[orderPhase] || 'Sales order missing!';


// partCatalogue
return row['Product'] && row['Product'].fields['partCatalogue'] || null;