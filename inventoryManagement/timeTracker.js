// Time tracker
const customer = row['Customer'] && row['Customer'].name || 'Customer';

const offerDate = (
    row['Offer date'] &&
    new Date(row['Offer date']).toISOString().substr(0,10) ||
    'Offer date'
);

return (offerDate + '/SO/' + customer);