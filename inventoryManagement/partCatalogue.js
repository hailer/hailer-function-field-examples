// Name
const component = row['Component'] && row['Component'].name || 'Component name';
const quantity = row['Quantity'] || 0;

return quantity + ' x ' + component;


// Total net price
const unitPrice = row['Component'] && row['Component'].fields['Unit_net_price'] || 0;
const quantity = row['Quantity'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(unitPrice * quantity) || null;


// Total weight
const unitWeight = row['Component'] && row['Component'].fields['Unit_weight'] || 0;
const quantity = row['Quantity'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(unitWeight * quantity) || null;


// Offered (SO)
const amount = row['Part of product'] && row['Part of product'].fields['Offered_(SO)'] || 0;
const quantity = row['Quantity'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(amount * quantity) || 0;


// Ordered (SO)
const amount = row['Part of product'] && row['Part of product'].fields['Ordered_(SO)'] || 0;
const quantity = row['Quantity'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(amount * quantity) || 0;


// Delivered (SO)
const amount = row['Part of product'] && row['Part of product'].fields['Delivered_(SO)'] || 0;
const quantity = row['Quantity'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(amount * quantity) || 0;


// Possible assemblies
const possibleAssemblies = (
    Math.floor(
        row['Component'] &&
        row['Quantity'] &&
        row['Component'].fields['In_stock'] / row['Quantity'] ||
        0
    )
);

return possibleAssemblies > 0 && possibleAssemblies || 0;