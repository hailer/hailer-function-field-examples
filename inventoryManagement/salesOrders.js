// Sales order
const customer = row['Customer'] && row['Customer'].name || 'Customer';

const offerDate = (
    row['Offer date'] &&
    new Date(row['Offer date']).toISOString().substr(0,10) ||
    'Offer date'
);

return (offerDate + '/SO/' + customer);


// Total price (excl. VAT)
const allowedName = ['Sales Order Rows'];
    
function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Total_price'] ||
        0
    );
})

return round(total) || 0;


// Total price (incl. VAT)
const tax = row['VAT'] && parseInt(row['VAT']) || 0;
const totalPrice = row['Total price (excl. VAT)'] || 0;

function round(input) {
    return Math.round(input * 100) / 100;
}

return round(totalPrice * (1 + (tax / 100)));


// Gross margin
const allowedName = ['Sales Order Rows'];
const totalPrice = row['Total price (excl. VAT)'] || 0;

let totalNet = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    totalNet += (
        allowedName.includes(activity.processName) &&
        (activity.fields['Unit_net_price'] * activity.fields['Quantity']) ||
        0
    );
})

return totalPrice && Math.round(100 * (totalPrice - totalNet) / totalPrice) || null;


// Total weight
const allowedName = ['Sales Order Rows'];

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Total_weight'] ||
        0
    );
})

return round(total) || 0;

        
// Time spent
const allowedName = ['Time Tracker'];

function round(input) {
    return Math.round(input * 100) / 100;
}

let total = 0;

Object.values(row.$.linkedFrom).forEach(activity => {
    total += (
        allowedName.includes(activity.processName) &&
        activity.fields['Time_spent'] ||
        0
    );
})

return round(total) || 0;


// orderRows
const processName = ['Sales Order Rows'];
const arrayOfObjects = [];

function stringify(data) {
    return JSON.stringify(data);
}

row.$.linkedFrom.forEach(item => {
    if (processName.includes(item.processName)){
        let object = {};
        object.name = item.name;
        object.activityId = item._id;
        object.processId = item.processId;
        object.phaseId = item.phaseId;
        object.salesOrder = (
            item.fields['Sales_order'] &&
            item.fields['Sales_order']._id  || ''
        );
        object.quantity = item.fields['Quantity'] || 0;
        object.product = (
            item.fields['Product'] &&
            item.fields['Product']._id  || ''
        );
        object.unitNetPrice = item.fields['Unit_net_price'] || 0;
        object.unitPrice = item.fields['Unit_price'] || 0;
        object.unitWeight = item.fields['Unit_weight'] || 0;
        arrayOfObjects.push(object);
    }
});

return stringify(arrayOfObjects);