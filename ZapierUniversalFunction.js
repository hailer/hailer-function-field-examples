// This is an attempt at a universal function for use in Zapier
// The function takes an Object of the form { var: fieldId } and as output
// gives the object back as { var: fieldValue }
let data = JSON.parse(inputData.data);
let fields = data.fields;

const fieldMapExample = {
    // Change the numbers to the field id
    numField1: "0",
    numField2: "1",
    textField: "2",
    // For a field with an Object as value and when you want a specific part of
    // that Object, put it's key as the value for target
    countryField: { id: "3", target: "country-code" },
    activitylinkField: { id: "4", target: "_id" }
}

function activityValues(fieldMap, fields) {
    Object.keys(fieldMap).forEach(key => {
        let found = false;
        Object.values(fields).forEach(field => {
            if (field.id == fieldMap[key]) {
                fieldMap[key] = field.value;
                found = true;
            } else if (field.id == fieldMap[key].id) {
                fieldMap[key] = field.value[fieldMap[key].target];
                found = true;
            }

        })
        if (!found) {
            fieldMap[key] = null;
        }
    })
    return fieldMap
}
