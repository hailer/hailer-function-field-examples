
function round(value, decimals){
    decimals = Math.pow(10,decimals);
    return Math.round(value * decimals) / decimals;
}
