/* 
    This example just demonstrates how you get the information "propagated"
    along your chain so it can later be reused.
    This needs to be done for each step in the chain that you need the data to go.
*/
return row['Sales Row'] && row['Sales Row'].fields['Linked_Information'] || '';
