let linked = row.$.linkedFrom;
let processId = 'processWeTakeInfoFrom';
let array = [];

Object.entries(linked).forEach(item => {

    if (item.processId === processId){
        let info = JSON.parse(item.fields['LinkedInformation']) || null
        array.push(info);
    }

})

return JSON.stringify(array);