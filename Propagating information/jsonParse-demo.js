let info = row['Sales_Order'] && row['Sales_Order'].fields['Linked_Information'] || null;
let data = null;
let profit = 0;
let cost = 0;
let output = 0;


function round(value, decimals){
    decimals = Math.pow(10,decimals);
    return Math.round(value * decimals) / decimals;
}


//Here we take the information that we now have available thanks to the "propagation" chain we created
if (info){
    data = JSON.parse(info);
}


Object.entries(data).forEach(item => {
    let profitable = ['process1', 'process2'];
    
    if (profitable.indexOf(item.type) != -1){
        profit += item.value;
    }
    else {
        cost += item.value;
    }

})


output = round(profit - cost,2);


return output;