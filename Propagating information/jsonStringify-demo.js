
/* 
Here is a quick demo on how to extract information, and then parse it into a string.

This has proven to be extremely useful in projects where we want to get information
to processes that are not directly linked

for example summarizing prices for a sales order that contains multiple steps and processes.


*/


const processIds = ['process1', 'process2', 'process3']; //these are the IDs we want to extract information from
const arrayOfObjects = []; //this is where we save the objects we create

function stringify(data) {
    return JSON.stringify(data);
}

function round(value, decimals){
    decimals = Math.pow(10,decimals);
    return Math.round(value * decimals) / decimals;
}

row.$.linkedFrom.forEach(item => {
    //first we check if this item is from one of our accepted processes
    let itemType = processIds.indexOf(item.processId);
    if (itemType != -1){
        let object = {};
        object.name = item.name;
        object.date = item.fields['Date'] || Date.now();
        //Here we first set all the variables that we know the objects have a common name
        //We then check which specific process it is, so we can get the information that particular process has
        if (itemType === 0){
            object.type = 'process1';
            object.value = round(item.fields['Sales_Price'] || 0, 2);
        }

        if (itemType === 1){
            object.type = 'process2';
            object.value = round(item.fields['Price'] || 0, 2);
        }
        if (itemType === 2){
            object.type = 'process3';
            object.value = round(item.fields['Purchase_Price'] || 0, 2);
        }

        //After we have stored all the information we need to an object, we can then push object into our array
        arrayOfObjects.push(object);
    }

});

return stringify(arrayOfObjects);

