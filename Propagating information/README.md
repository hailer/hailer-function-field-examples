## Data propagation ##

In this example we use JSON.stringify and JSON.parse
to extract, move, and use information

This has come in quite handy in our customer projects to collect information from different activities
into one text area, which we then "move" with function fields in a sense that we re-print them in hidden function fields

by hidden function fields we mean that we don't actually use them in any phase of a process/dataset, 
but they still calculate their value, and thus can be read from linked activities

Then all we have to do is just move the information along our "chain" and use it where needed to calculate/display the information we have.
  
