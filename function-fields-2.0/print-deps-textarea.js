const dep = { "Customer": "Nicolina Hailerino", "Email": "nicolina.hailerino@hailer.com", "Phone": "0400123123", "Adress": "Testroad 12 A 5", "Postal Code": "00580", "City": "Helsinki" };
let output = '';

// This loops through all parameters that are found in the dep object, and adds the title, and value to our output, each on their own line
Object.entries(dep).forEach(([param, value]) => {
    output += param + ': ' + value + '\n';
})

console.log(output);
//return output;

/* 
This will print out the following:

Customer: Nicolina Hailerino
Email: nicolina.hailerino@hailer.com
Phone: 0400123123
Adress: Testroad 12 A 5
Postal Code: 00580
City: Helsinki

*/