const dep = {
    weight: [1,2,3,4,7,123,999] //Linked from activitites one certain field.
}

return dep.weight && Math.round(dep.weight.reduce((x,y) => x + y) * 100) / 100 || null;

/* 
here we iterate each number that is in the weight array,
and add it together

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce

*/