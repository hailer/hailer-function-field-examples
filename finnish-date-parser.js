

/*  timestamp: time in milliseconds,
    includeYear: boolean (true/false, 1/0), can also be left empty */
function finnishDateString(timestamp, includeYear) {
    const date = new Date(timestamp);
    let month = ((date.getMonth() < 10) 
        ? '0'+ (date.getMonth()+1 )
        : (date.getMonth()+1)
    );
    let year = ((includeYear) 
        ? `.${date.getFullYear().toString().substr(-2)}`
        : '' );
    let output = (`${date.getDate()}.${month}${year}`);
    return output;
};


// Print date with year
console.log(finnishDateString(Date.now(),1));

// Print only date
console.log(finnishDateString(Date.now()));

//example: 
let date = Date.now()
let dateString = finnishDateString(date, true);

console.log ('Hello, todays date is: ', finnishDateString(date ,1));