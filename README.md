# README #

This repository is a collection of Function Fields and demos
that the Hailer team and other collaborators have found useful

### What is this repository for? ###

* Quick access to code examples you can freely reuse
* Learning new ways of writing Javascript
* Helping you create your own functions





### How do I get set up? ###

These examples will not be to just "copy paste and it works".
They are meant for the user to read through and understand.

Your Hailer workspace will most likely not use the same
field names etc, which means you will have to usually make small changes.

We're trying to keep all examples commented on how each function and thing works,
and will try our best to explain how to understand and reuse the code

### To try out any of these examples ###
 
You can open your browsers console and run it from there (f12 on chrome).
Keep in mind that `return yourThing;` 
does not work in the console, 
to see what it outputs you have to use `console.log(yourThing);`
this is also very useful for debugging and testing outside of your function field.


### Contribution guidelines ###

We strongly encourage that if you have a function field example
that you can't find in here, we would very much like to add it.

To contribute to this repository:

* create a pull request, guide can be found [ here ](https://www.atlassian.com/git/tutorials/making-a-pull-request)


If you're not at all familiar with how git works, we strongly recommend that you try and learn it, It's an amazing tool for keeping track of your work. (can be used for other things than code). [What is Git?](https://www.youtube.com/watch?v=2ReR1YJrNOM) is a short video what it does. [Here](https://www.youtube.com/watch?v=USjZcfj8yxE) is a 15 minute video explaining the basics on how to use it.

If you can't manage to create a pull request, you can still contribute through
sending the code to [Carl-Oscar](mailto:carl-oscar.lindfors@hailer.com) and i'll add it



