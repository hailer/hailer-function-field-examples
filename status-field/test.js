
let deniedIds = [
    'item1', // TODO phase
    'item2', // In progress phase
    'item3'  // inspection pending phase
    ];
let acceptedPhaseId = 'acceptedId'; // Inspection DONE
let green = "✅";
let red = "🛑";
let yellow = '⚠️';
let maxDays = 604800000;    // 7 days in milliseconds
let yellowDays = 345600000; // 3 days in milliseconds
let status = green;
let lastInspectionDate = 0;

Object.values(row.$.linkedFrom).forEach(value => {

    if (value.phaseId === acceptedPhaseId){
        lastInspectionDate = value.fields['Date'] || 0;
    }

    if (deniedIds.indexOf(value) != -1){
        status = red;
    }
})

if (status != red) {
    let timePassed = Date.now() - lastInspectionDate;
    if (timePassed > yellowDays){
        (timePassed >= maxDays) ? status = red : status = yellow;        
    }
}

return status;