## Status Check ##
In this example, we're using linked activities from a product to determine the status using "emotes"  
  
if the product has things that are in todo, in progress or pending inspection we want to  
> return: red
  
this product also requires a weekly inspection, so im also checking when the last succesful inspection was.  
  
if its 7 days or more since the last inspection, we want to
> return: red

if its between 3 and 7 days i want to
> return: yellow 

if none of my criterias are being met it will return default.
> return: green
  

### How this works ###

```
if (deniedIds.indexOf(value) != -1){
    status = red;
}
```
this will see if the linkedFrom activity is apart of my "deniedIds",  
keep in mind this is within a forEach loop, which means it will do this check for all linked activities
  
  
  
``` 
if (status != red) {
    let timePassed = Date.now() - lastInspectionDate;
    if (timePassed > yellowDays){
        (timePassed >= maxDays) ? status = red : status = yellow;        
    }
} 
```
Lastly, once we have checked all linkedFrom, we check what the last inspection date is.  
The linkedFrom activities are always created in order, so the item that was linked most recently, is at the bottom.  
This means that even tho our lastInspectionDate changes for each linked activity that has the correct phaseId,
we can still trust that the newest, is the latest one.

So no we just check the time between now, and last inspected, and return the corresponding status.  
(This is all with epoch timestamp, so we are counting milliseconds, this might sound overkill but it's how javascript handles time, so it's the most efficient way).