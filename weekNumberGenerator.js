function getWeekNumber(date) {
    var utcDate = new Date(date);
    // Thursday in current week decides the year according to Gregorian calendar
    let getThursday = (utcDate.getDate() + 3) - (utcDate.getDay() + 6) % 7;
    utcDate.setDate(getThursday);
    // January 4 is always in week 1.
    var week1 = new Date(utcDate.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(
      ((utcDate.getTime() - week1.getTime()) 
      / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7
    );
}

/*  these variables are for demo purposes
    the function requires the date to be an epoch timestamp
    
    if your date is instead in UTC format
    you can remove line 2 and change all "utcDate" to "date" 
*/

let epochDate = Date.now();
let weekNumber = getWeekNumber(epochDate);

//this console log is if you want to run this code locally or your browser console
console.log(weekNumber);

return weekNumber;